/*
 * Copyright 2024 connect2x GmbH
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

plugins {
    alias(libs.plugins.androidLibrary) apply false
    alias(libs.plugins.kotlin.multiplatform) apply false
    // For sample
    alias(libs.plugins.androidApplication) apply false
    alias(libs.plugins.compose) apply false
    alias(libs.plugins.compose.compiler) apply false
}

val baseVersion: String = libs.versions.sysnotify.get()

allprojects {
    group = "de.connect2x"
    version = withVersionSuffix(baseVersion)

    dependencyLocking {
        lockMode = LockMode.LENIENT
        lockAllConfigurations()
    }

    repositories {
        mavenLocal()
        mavenCentral()
        google()
        maven("https://gitlab.com/api/v4/projects/65231927/packages/maven") // kmp-jni
    }
}
